import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { TabsPage } from "../tabs-page/tabs-page";

/**
 * @author SOUM Som On
 */
@Component({
  selector: 'page-create-account',
  templateUrl: 'create-account.html',
})

export class CreateAccountPage {
  constructor(public navCtrl: NavController) {

  }

  onGoToUniversity() {
    this.navCtrl.setRoot(TabsPage)
  }
}
