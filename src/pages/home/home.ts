import { Component, OnInit } from '@angular/core';
import { MenuController } from 'ionic-angular';

/**
 * @author SOUM Som On
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  constructor(public menuCtrl: MenuController) {

  }
  ngOnInit() {
    this.menuCtrl.enable(true);
  }
}
