import { Component } from "@angular/core";
import { HomePage } from "../home/home";
import { SettingPage } from "../setting/setting";
import { AboutPage } from "../about/about";
import { NavParams } from "ionic-angular";
import { HelpPage } from "../help/help";

/**
 * @author SOUM Som On
 */
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs-page.html'
})

export class TabsPage {

  //set the root page for each tab
  tab1Root: any = HomePage;
  tab2Root: any = HelpPage;
  tab3Root: any = AboutPage;
  tab4Root: any = SettingPage;
  myselectIndex: number;

  constructor(public navParams: NavParams) {
    this.myselectIndex = navParams.data.tabsIndex || 0;
  }
}
