import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CreateAccountPage } from "../create-account/create-account";
import { TabsPage } from "../tabs-page/tabs-page";
import { UserData } from "../../assets/interface/user.data";
import { NgForm } from "@angular/forms";
import { MenuController } from 'ionic-angular';

/**
 * @author SOUM Som On
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  user: UserData = { username: '', password: '' };
  inputUsername: string = 'on';
  inputPassword: string = '123';

  constructor(public navCtrl: NavController, public menuCtrl: MenuController) {

  }

  /**
   *
   */
  onGoToUniversity(form: NgForm) {
    if (form.valid) {
      if (this.user.username === this.inputUsername && this.user.password === this.inputPassword) {
        this.navCtrl.setRoot(TabsPage);
      }
    }

  }

  /**
   *
   */
  onGoToCreateAccount() {

    this.navCtrl.push(CreateAccountPage);
  }
  ngOnInit() {
    this.menuCtrl.enable(false);
  }
}
