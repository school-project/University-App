import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { AboutPage } from "../pages/about/about";
import { TabsPage } from "../pages/tabs-page/tabs-page";
import { AccountSetting } from "../pages/account-setting/account-setting"

/**
 * @author SOUM Som On
 */

@Component({
  templateUrl: 'app.html',
})

export class MyApp {
  @ViewChild(Nav) nav: NavController;

  rootPage: any = LoginPage;

  listPages: Array<{ title: string, component: any, icon: string }>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen, ) {
    this.initializeApp();

    this.listPages = [
      { title: 'Home', component: TabsPage, icon: 'home' },
      { title: 'About Us', component: TabsPage, icon: 'information-circle-outline' },
      { title: 'Account Setting', component: TabsPage, icon: 'construct-outline' },
      { title: 'Logout', component: LoginPage, icon: 'log-out' },
    ];
  }

  /**
   *
   */
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

