import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {LoginPage} from "../pages/login/login";
import {TabsPage} from "../pages/tabs-page/tabs-page";
import {AboutPage} from "../pages/about/about";
import {SettingPage} from "../pages/setting/setting";
import {HelpPage} from "../pages/help/help";
import {CreateAccountPage} from "../pages/create-account/create-account";
import {AccountSetting} from "../pages/account-setting/account-setting"
import {UniversityDetailPage} from "../pages/university-detail/university-detail";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    AboutPage,
    SettingPage,
    HelpPage,
    CreateAccountPage,
    AccountSetting,
    UniversityDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    AboutPage,
    SettingPage,
    HelpPage,
    CreateAccountPage,
    AccountSetting,
    UniversityDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {

}
